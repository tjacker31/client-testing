const exitintel = require('../scripts/exitintel'), path = require('path');
const filename = path.parse(process.argv[1]).name;
const url = 'https://taftclothing.com/';

exitintel.tests.onEntrance({
	testerInitials: process.argv[3] || '',
	name: filename,
	url: url,
	option: process.argv[2] || '',
	ad: '#exitintel-debug:[{"name":"reset"},{"name":"preview","campaignId":"595d15d75d226bcc00000004","adId":"5a9f0d363dd8245020000000"}]',
	steps: 3,
	elements: {
		yes: 'yes-free-stuff',
		submit: 'enter-now',
		continue: 'continue-shopping'
	}
});