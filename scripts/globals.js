module.exports = {
	dateFormat: 'MMDDYY_HHmmss',
	domain: '@testexitintel.com',
	path: '../clients/screenshots',
	config: {
		ignoreHTTPSErrors: true,
		slowMo: 200,
		devtools: true
	},
	viewport: {
		width: 1200,
		height: 900,
		deviceScaleFactor: 1
	},
	navigationState: {
		waitUntil: 'load' ,
	},
	master: '#exitintel-debug:[{"name":"reset"},{"name":"pub","value":"master-preview"}]',
	popup: {
		className: '.exitintel-panel',
		label: 'data-exitintel-label'
	},
	message: {
		timeout: 'ERROR: Testing timed out and could not complete.',
		load: 'ERROR: Website failed to load.',
		ad: 'WARNING: An ad did not fire. See if there is a control group.'
	}
};

// TODO: For ad warning, should just be able to see if control value is in cookie 