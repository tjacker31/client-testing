const puppeteer = require('puppeteer'), fs = require('fs'), moment = require('moment'), globals = require('./globals');
const devices = require('puppeteer/DeviceDescriptors');
const timestamp = moment.utc().format(globals.dateFormat), domain = globals.domain;

// Create a directory within project folder for screenshots if none exits
createDirectory = (name) => {
	let path = globals.path;
	try {
		fs.mkdirSync(`${path}/${name}`);
		console.log('Client screenshot directory created.');
		return `${path}/${name}`;
	} catch (err) {
		if (err.code === 'EEXIST') {
			return `${path}/${name}`;
		} else {
			console.log('ERROR: Could not create client directory.');
			return err.code;
		}
	}
};

// Export object containing tests methods
module.exports.tests = {
	onEntrance: (client) => {
		const name = client.name;
		// Set directory with client ID and name
		let path = createDirectory(name);
		const initials = client.testerInitials; 
		const className = globals.popup.className, label = globals.popup.label;
		const el = client.elements;
		// Set path and file naming convention for screenshots
		const pngFile = `${path}/${timestamp}_on_entrance_step_`;
		// Screenshot step count
		let i = 0;

		// Create a chromium browser instance
		puppeteer.launch(globals.config).then(async browser => {

			const page = await browser.newPage();
			browser.userAgent().then(res => console.log(res)).catch(err => console.error(err));
			
			// Configure viewport and set load option
			await page.setViewport(globals.viewport);
			await page.goto(client.url, globals.navigationState).catch(err => console.log(globals.message.load));

			// TODO: Find out when exitintel cfid cookie is set. Needed to stop test for control group
			// await page.cookies().then(cookies => cookies);

			// Add custom styling to highlight element placement on ad
			await page.addStyleTag({path: '../include/custom.css'});

			// If block for when ad includes a yes/no step
			if (client.steps === 3) {
				// Wait for yes button and click after screenshot
				await page.waitForSelector(`${className} button[${label}="${el.yes || 'yes-button'}"]`, {
					visible: true,
				}).catch(err => console.log(globals.message.ad));
				await page.screenshot({path: `${pngFile}${++i}.png`});
				await page.click(`${className} button[${label}="${el.yes || 'yes-button'}"]`);
			}

			// Wait for email input field and automatically type test email. Submit after screenshot
			await page.waitForSelector(`${className} input[${label}="${el.email || 'input-email'}"]`, {
				visible: true,
			});
			await page.type(`${className} input[${label}="${el.email || 'input-email'}"]`, `${initials}${timestamp}${domain}`);
			await page.screenshot({path: `${pngFile}${++i}.png`});
			await page.click(`${className} button[${label}="${el.submit || 'submit-button'}"]`);

			// Wait for continue button and click after screenshot
			await page.waitForSelector(`${className} button[${label}="${el.continue || 'continue-button'}"]`, {
				visible: true,
			});
			await page.screenshot({path: `${pngFile}${++i}.png`});
			await page.click(`${className} button[${label}="${el.continue || 'continue-button'}"]`);

			// Log when test is complete and close browser instance
			console.log('Test complete.')
			await browser.close();
		}).catch(err => globals.message.timeout);
	}
};